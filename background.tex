\section{Background and Motivation}
\label{sec:background}
In this section, we first give a short review of the LTE/5G modulation scheme, frame structure, and channel estimation mechanism. We then provide a brief description of the SigOver attack, followed by the motivation for using channel estimates to detect signal injection attacks.

\subsection{LTE/5G Physical Layer Measurements}
\label{sec:lte}
\noindent
\textbf{Modulation Scheme and Frame Structure}. LTE and 5G use the orthogonal frequency-division multiple access (OFDMA) modulation scheme to schedule multiple users across time and frequency resources. In this paradigm, the fundamental unit of resources is one sub-carrier for one OFDM symbol duration, or a single resource element (RE). Base stations (BSs) schedule downlink and uplink REs for endpoints (a.k.a, user equipment (UEs)) in a structured radio frame system, where each radio frame is 10 ms long and contains 10 subframes, each lasting 1 ms. Each subframe contains a number of OFDM symbols. In 5G the OFDM symbol duration and sub-carrier spacing can vary, but these parameters are fixed in LTE, where each subframe contains 14 OFDM symbols. In both cases, resources are organized into several logical and physical channels serving various needs, e.g., synchronization, broadcasting system information, paging, transferring user-plane data, etc.

\noindent
\textbf{Channel Measurements and Equalization}. The multipath nature of the wireless channel will cause a receiver to experience multiple time-shifted and attenuated versions of the transmitted signal. In order to compensate for such distortions, OFDM uses a subset of resource elements to send known pilot symbols that the receiver can use to estimate the effects of the channel and compensate for them in order to correctly demodulate the other symbols. The effects of the channel can be modeled as a channel impulse response (CIR) or its Fourier pair, the channel frequency response (CFR). The effects of the channel can be represented as $y = h \ast x + n$ or $Y = H X + N$, where $y$ and its Fourier transform $Y$ represents the received signal, $x$ and its Fourier transform $X$ represents the transmitted signal, $h$ and its Fourier transform $H$ represent the CIR and CFR, and $n$ and its Fourier transform $N$ represent noise. Since OFDM/OFDMA modulates symbols in the frequency domain, the CFR is estimated for each OFDM symbol and used to correct for the effects of the channel.

\subsection{Signal Overshadowing Attack}
In order to successfully overshadow specific OFDMA resources, the attacker must first achieve accurate time and frequency synchronization with the legitimate BS. After that, the attacker crafts malicious subframes by altering the frequency domain data symbols in the original messages, while preserving the pilot symbols used for channel estimation at the victim UE, then transmitting a time domain signal that precisely overlaps with the desired subframe. The precise synchronization and manipulation ensures that the altered messages can be decoded correctly by the victim. The attacker then transmits the counterfeit subframe from a position, and at a power level, that ensures it will be at least 3 dB higher than legitimate subframe at the victim UE. Figure~\ref{fig:sigover} illustrates the attack in the presense of \Name{} detectors. If the attacker is well synchronized with the legitimate BS and knows the precise relative distances between itself, the BS, and the victim, it can further improve the time synchronization of the legitimate and malicious signals arriving at the victim UE. However, \Name{} can potentially leverage that fact that the time synchronization is not likely to be as good at the detectors in this circumstance because they occupy different relative positions to the BS and attacker.
 
\subsection{Motivation}
%To effectively identify low-power signal injection attacks on victims and nearby nodes, we must address two questions: (1) Can a <=3 dB increase due to attacks be dependably detected using the RSS indicator? (2) Are there any more reliable indicators available for detection?
To motivate the use of a CFR-based detection method like \Name{}, we first need to understand why a detector based on simpler and more readily available channel metrics will not meet the challenges of signal injection attacks like SigOver. RSSI measurements, e.g., are already made available at the application layer in commercial off-the-shelf (COTS) handsets (although not necessarily at subframe granularity), making them attractive for developing a detector, as it could be deployed as an iOS/Android application to many devices right now.

In order to understand whether RSSI fluctuations alone could lead to a reliable detector, we used an Ettus B210 SDR~\cite{B210} and srsRAN~\cite{srsran} (an open-source LTE implementation) to study the fluctuations of RSSI measurements made by endpoints camped on commercial LTE cells. We collected data at various locations and times, in scenarios where the channel would be less dynamic (e.g., late at night with a stationary endpoint) and scenarios where where the channel would vary more (e.g., daytime and with a mobile endpoint).
Figure~\ref{fig:rssi_env} shows the distribution of RSSI changes ($D_{RSSI}$) between consecutive subframes in these scenarios. (Subrame-to-subframe variations are of interest here because that is the granularity of the attack in question.) We observe that even when endpoint devices are stationary, RSSI measurements can change by up to 7.5 dB between continuous subframes, while attacks like SigOver can be succeed while causing smaller deltas (3 dB at the victim; maybe less at neighboring detectors), making them challenging to detect reliably with RSSI alone. Such substantial fluctuations are not uncommon in cellular networks. In addition to the effects of normal channel variations, we find (see Section~\ref{sec:fairness}) that RSSI measurements are roughly proportional to the amount of radio resources scheduled in a given subframe, which can vary significantly from one subframe to the next. In cells with wider bandwidth, RSSI fluctuations become even more pronounced. This motivates a detector that can leverage more discriminative measurements.

% \iffalse
% Channel response estimates describe how the wireless channel distorts signals as they propagate from transmitter to receiver, and are used to correct these distortions at the receiver and correctly demodulate the transmitted information.
% The channel response can be expressed in both the time and frequency domains, using the channel impulse response (CIR) and channel frequency response (CFR), respectively.
% The two channel responses are described by the following equations:

% \begin{align}
% \label{eq:CFR}
% y &= h \ast x  + w\\
% Y &= H X + W
% \end{align}
% where $y$ and its Fourier transform $Y$ represent received signals, $x$ and its
% Fourier transform $X$ represent transmitted signals, $w$ and $W$ represent white
% Gaussian noise, and $h$ and $H$ represent the CIR and CFR, respectively. In LTE and 5G,  known reference signals (RS) are transmitted periodically, allowing the receiver (BS or UE) to estimate the channel and mitigate its effects.

% \begin{figure}
%      \centering
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/indoor_amp.pdf}
% 	\caption{Indoor CFR amplitude}
%      \end{subfigure}
%      \hspace{0.01mm}
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/indoor_phase.pdf}
%          \caption{Indoor CFR phase}
%      \end{subfigure}
%           \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/outdoor_amp.pdf}
% 	\caption{Outdoor CFR amplitude}
%      \end{subfigure}
%      \hspace{0.01mm}
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/outdoor_phase.pdf}
%          \caption{Outdoor CFR phase}
%      \end{subfigure}
%      \caption{The impact of SigOver on the amplitude and phase of CFR in indoor and outdoor scenarios for an affected subframe (red) and two normal subframes (blue and green) ahead of it.}
%      \label{fig:perturbation}
% \squeezeup
% \end{figure}
% \fi
%Since attackers cannot forge the channel, channel responses are commonly used in spoofing detection to distinguish transmitter sources. SigOver has also emphasized the usefulness of CFR amplitude shape for detecting anomalies, but there are limitations NLOS scenarios. However, we observed in our experiments that SigOver not only affects CFR amplitude but also CFR phase, as shown in Figure~\ref{fig:perturbation}. Therefore, we were inspired to explore all potential channel response features and use them collectively for anomaly detection.


%Since attackers cannot forge the channel, and the channel varies significantly over short distances in many cases, channel responses are commonly used to distinguish transmitters and detect spoofing attacks.While the authors of SigOver suggest that changes in CFR amplitude may serve as a potential indicator, they also demonstrate its ineffectiveness in non-line-of-sight (NLOS) scenarios. However, we expect that there may be better CFR-based indicators for these kind of attacks, e.g., phase perturbations (Fig.~\ref{fig:perturbation}), an example showing during an attack, the CFR phase 比 amplitude展现出更明显的不正常的样子 so we explore a variety of potential channel response features and use them collectively to come up with a more robust detector.

%Channel responses are commonly used to distinguish transmitters and detect spoofing attacks, since attackers cannot forge the channel, and the channel varies significantly over short distances in many cases. However,  detecting attacks with low transmission power in cellular network presents unique challenges. The difficulty mainly arises from the dynamic nature of wireless channels. These channels are often affected by various factors such as physical obstacles, and movement of objects, which can cause channel fluctuation. Though when attack with high transmission power, the perturbations in the channel from the attack are more noticeable than those natural channel fluctuation, it is hard to tell them apart from each other when the attack signal strength is low on the detectors.
%The authors of SigOver  suggest that changes in CFR amplitude may serve as a potential indicator, but also mentioned the challenge to use it in NLOS scenarios.  To address this limitation, we investigate alternative CFR-based indicators. Fig.~\ref{fig:perturbation} illustrates that, in our outdoor NLOS experiments, the CFR phase displays more pronounced abnormalities  during an attack than its amplitude (not an isolated case), making it a potentially more discriminative characteristic. To fully leverage the potential of CFR data, we conduct a comprehensive exploration of various channel response features and synergistically combine them to create a more robust detector. 
\begin{figure}
\centering
\includegraphics[width=0.45\textwidth]{fig/rssi_env.pdf}
\caption{ Distribution of RSSI variation between two contiguous subframes, measured at different locations, time of day, and environment, having different shapes and ranges, from a commercial cell with a 5 MHz bandwidth at a center frequency of 1967.5 MHz for 100ms.}
\label{fig:rssi_env}
\squeezeup
\end{figure}
\begin{figure}
     \centering
      \subfigure[Outdoor CFR amplitude]{\includegraphics[width=40mm]{fig/outdoor_amp.pdf}}
      \subfigure[Outdoor CFR phase]{\includegraphics[width=40mm]{fig/outdoor_phase.pdf}}
      \caption{The impact of SigOver on the amplitude and phase of CFR in outdoor scenario for an affected subframe (red) and two normal subframes (green and blue) ahead of it.}
      \label{fig:perturbation}
\squeezeup
\end{figure}

Channel estimates and/or features derived from them represent an ideal target for detecting attacks like SigOver for a variety of reasons. Among other reasons: (a) they are calculated for every subframe in LTE/5G in order to address expected channel variations; (b) they represent a richer data source than RSSI; and (c) they are bound to be affected by attacks which transmit the same pilots from different locations because of the nature of the multipath channel, which leads to significant differences in the channels witnessed by receivers separated by fairly short distances. I.e., even a well-synchronized attack signal will arrive at the receiver having traveled different paths through the environment, interfere with the legitimate signal, and result in perturbations in its CFR estimates.

For these reasons, channel estimates have often been employed to identify transmitters and recognize spoofing attacks in other types of wireless networks. However, the effectiveness of using channel estimates to detect low-power injection attacks on LTE/5G networks remains unclear and, to the best of our knowledge, has not been thoroughly explored in the literature. The authors of SigOver pointed to channel estimation techniques as a possible solution and described some of the challenges involved, showing some evidence that it might be inadequate to depend solely on CFR amplitude changes for detection in non-Line-of-sight (NLOS) conditions, but left the design of a robust CFR-based detection for future work.

This work picks up that torch and thoroughly explores the effectiveness of leveraging channel estimate perturbations in order to detect these attacks. As an initial example, we note that CFR phase estimates may aid in detection in scenarios where the amplitude alone might not be effective alone. Fig.~\ref{fig:perturbation} shows channel estimates that were captured in one of our outdoor experiments, normal and during an attack, which included scenarios with LOS and NLOS links in a dynamic urban environment. To fully exploit the effects of attacks on CFR estimates, we perform an extensive study of various channel response attributes and how they react to signal injection attacks under a variety of channel conditions, eventually arriving at an effective and resilient detection mechanism.

% \iffalse
% This section begins with a concise overview of the LTE network architecture and a formalization of channel estimation, which will be utilized in Sec.~\ref{sec:detection} to explain the \Name{} approach. We will also provide a brief introduction to SigOver~\cite{sigover}.

% \subsection{LTE Basics}
% The LTE network infrastructure is designed to provide Internet access to users through the use of end devices called User Equipment (UE), intermediate connectors known as Evolved NodeB (eNodeB) base stations, and a core network called Evolved Packet Core (EPC) which responsible for mobility management. These components work together to establish a reliable connection for users.

% \paragraph{ Radio Frequency Signals.}
% The UE and eNB communicate with each other using radio frequency (RF) signals that are transmitted and received over an air interface. The frequency domain representation of the RF signal is its spectrum, which shows the distribution of power over the range of frequencies that the signal occupies.
% Before being transmitted, RF signals are typically generated in the frequency domain and then modulated to the time domain using OFDM.
% In the time domain, the RF signal can be represented by its amplitude or power over time.
% To structure the transmission of the RF signal in the time domain, radio frames are used, each lasting 10 milliseconds (ms) and consisting of 10 subframes of 1 ms each. Each subframe is divided into two equally long slots, each containing seven OFDM symbols. This structure is used to efficiently transmit and receive data over the air interface between the UE and eNB in a cellular communication system.


% \paragraph{Receive Strength Signal.}
% Receive Strength Signal Indicator (RSSI) is a measure of the average power of the signal received by an antenna over a specific measurement bandwidth.  The measurement is based on data from the whole bandwidth and  is calculated using OFDM symbols that contain reference symbols and includes the power of the signal including reference symbols and traffic from the serving cell as well as interference and noise from other sources.

% \iffalse
% \paragraph{Carrier frequency Offset.}
% Carrier frequency offset (CFO) is the difference between the frequency of the carrier signal transmitted by the sender and the frequency at which it is received by the receiver. This difference can be caused by various factors such as the Doppler effect due to the relative movement between the transmitter and receiver, the oscillator's frequency drift over time, or the frequency error of the oscillator used in the transmitter or receiver.
% \fi

% \paragraph{Channel Response.}
% \label{Channel Information}

% The communication channel can cause various distortions in the shape and strength of a received signal, such as fading, scattering, power decay, and noise. To correct for these distortions, the receiver must estimate the overall effect that the channel has on the signal as it is transmitted, known as the channel response. This can be represented in both the time and frequency domains, using the channel impulse response (CIR) and the frequency response (CFR), respectively. The two channel responses can be estimated by the following equations:
% \begin{align}
% \label{1}
% y &= h \ast x  + w\\
% Y &= H X + W
% \end{align}

% where $y$ and its Fourier transform $Y$, $x$ and its Fourier transform $X$, are the received and transmitted reference signals (RS) which are designed to be known by both UE and eNB for the purpose of channel estimation in the time and frequency domains. $w$ and $W$ represent white Gaussian noise, and $h$ and $H$ represent the CIR and CFR, respectively.

% \iffalse
% Since signal injection attacks do not contaminate RS, when malicious signals overlay legitimate signals, the superimposed channel can be written as Equation \ref{2},
% where $H$ and $H_a$ are the channel frequency responses of the original cell and the attacker. $y'$ and $Y'$ are the composite RS.
% \begin{align}
% \label{2}
% \begin{split}
% y' &= h \ast x  + h_a\ast x + w\\
% Y' &= H X + H_a X + W
% \end{split}
% \end{align}
% \fi

% \subsection{Signal Overshadowing Attack}

% %SigOver, the first published successful signal injection attack associated with cellular networks, directly manipulates LTE broadcast messages by overlaying the original broadcasting message with a counterfeited one to achieve a variety of~\cite{sigover}.\footnote{SigOver is applied to the LTE-Frequency Division Duplex (FDD) mode, which is widely used by mobile operators across te globe~\cite{global2018evolution}. While we evaluate \T{\Name{}} in an FDD system in order to detect SigOver, we expect similar performance for TDD systems.} The adversary in this attack synchronizes in time and frequency to a legitimate base station and eavesdrops on downlink broadcast messages in order to forge a malicious subframe under the legitimate message framework.


% To perform a signal overshadowing attack with a high success rate, an attacker follows a series of steps including synchronizing tightly with a cell in the network to obtain its time clock, crafting malicious subframes using Information Elements (IEs), and injecting them into the network with an at least 3 dB higher transmission power than legitimate subframes. These malicious subframes, when received and decoded by victim devices, will exhibit the intended behavior of the attacker. The target of the attack can be a group of victims through the injection of malicious subframes into System Information Block (SIB) subframes, or a specific individual through the injection of subframes into paging subframes. Many attack scenarios can be implemented using signal overshadowing, and the authors of this work have shared the source code for multiple such scenarios. In the development of a detection method, the focus is on the type of attack being carried out rather than the specific scenario being used, so the paging with IMSI scenario was chosen as a model.

% Paging with IMSI attack scenario is a type of signal overshadowing attack that exploits the rules of paging mobility management in a wireless communication network. In this scenario, the attacker injects malicious paging messages into the network, using either the permanent identity (IMSI) or temporary identity (S-TMSI) of a specific target UE (User Equipment). Paging messages are typically used to notify a particular UE of the arrival of downlink data and to establish a communication connection. Normally, after a UE camps on a cell, it obtains an S-TMSI to replace its more private IMSI identifier. However, if there is a network failure or an S-TMSI allocation failure, a paging message with IMSI may be transmitted. The SigOver attack can exploit this by injecting paging messages with the target's IMSI repetitively, causing the UE to continuously reattach to the cell and potentially causing a Denial of Service (DoS) attack.


% %Fig~\ref{attack_model} illustrates the signal overshadowing attack. Specifically, time is divided into 1-ms subframe units. Broadcasting messages carrying cell configuration and other downlink shared information is periodically allocated to specific subframes. A SigOver attacker synchronizes with a cell to get the cell's time clock, delicately crafts malicious subframes, and injects the fake subframes with a slightly higher transmission power (about 3 dB from capture effect theory hl{capture effect}) to overshadow the legitimate subframe. Such manipulated, legitimate-looking messages can be correctly decoded by the victim UEs, yielding the behavior intended by the adversary.

% %The adversary crafts the subframes by filling in particular Information Elements (IEs) with malicious information to accomplish diverse attack goals for a one or more UEs within the attack range. Table~\ref{attacks} shows the variety of potential attacks. Five attack scenarios can be divided into two categories according to the target. The attack aims at a group of victims when the injection is on the System Information Block (SIB) subframes. When SigOver is on paging subframes, a specific individual is targeted. For more details about the implementation of each attack scenario refer to~\cite{sigover}. The authors of this work were willing to share the source code for the overshadowing of paging messages with IMSI, so we used that as a model for our detection method.

% %Paging messages, a typical broadcasting instance, notifying a particular UE of the downlink data arrival to establish a communication connection, attach either a permanent identity (IMSI) or a temporary identity (S-TMSI) of the UE assigned by the cell. Normally, after a UE camps on a cell, it obtains its S-TMSI to replace the more private IMSI identifier. Only when there is a network failure or an S-TMSI allocating failure will a paging message with IMSI be transmitted. Thus, SigOver can keep a UE reattaching a cell to achieve a Denial of Service (DoS) attack by injecting paging messages with the target's IMSI, if known, repetitively.



% \subsection{Motivation}
% \label{sec:motive}
% To detect SigOver attacks, we must first address two questions: (1) Can a 3 dB increase in signal strength be detected? (2) Are there better indicators that can reflect the presence of an attack?

% To investigate signal strength variation, we utilized RSS as previously introduced. In order to verify the typical RSSI variation, we gathered distributions of RSSI variation $D_{rssi}$ from a B210 software-defined radio (SDR) device connected to a commercial network. The measurements were taken at three distinct locations, two durations, and various scenarios. As shown in Figure ~\ref{fig:rssi_env}, the maximum fluctuation of RSSI can reach up to 7.5 dB. The natural instability in RSSI may obscure the 3 dB increase in signal strength caused by SigOver.

% \begin{figure}[h]
% \centering
% \includegraphics[width=0.49\textwidth]{fig/rssi_env.pdf}
% \caption{ Distribution of RSSI variation between two contiguous subframes, measured at different locations, time of day, and environment, having different shapes and ranges, from a cell with a 5MHz bandwidth at a center frequency of 1967.5MHz for 100ms.}
% \label{fig:rssi_env}
% \end{figure}

% The channel cannot be forged by attackers, so spoofing detection can use CFR or CIR for detecting anomalies. SigOver has also highlighted the usefulness of CFR amplitude shape for detection, but it has limitations in NLOS scenarios. We observed that SigOver can cause perturbations in both CFR amplitude and phase, as shown in Figure~\ref{fig:perturbation}. The changes in the CFR amplitude shape of the attacked subframe may not be easily noticeable, but the phase changes are significant. Hence, we were inspired to investigate all possible CFR features and utilize them collectively for detection.


% \begin{figure}
%      \centering
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/indoor_amp.pdf}
% 	\caption{Indoor CFR amplitude}
%      \end{subfigure}
%      \hspace{0.01mm}
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/indoor_phase.pdf}
%          \caption{Indoor CFR phase}
%      \end{subfigure}
%           \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/outdoor_amp.pdf}
% 	\caption{Outdoor CFR amplitude}
%      \end{subfigure}
%      \hspace{0.01mm}
%      \begin{subfigure}[b]{0.23\textwidth}
%          \centering
%          \includegraphics[width=\textwidth]{fig/outdoor_phase.pdf}
%          \caption{Outdoor CFR phase}
%      \end{subfigure}
%      \caption{The perturbation on CFR amplitude and phase caused by SigOver in indoor and outdoor scenario.}
%      \label{fig:perturbation}
% \end{figure}
% \fi


% \iffalse
% SigOver surpasses other spoofing attacks, such as the FBS or MITM, in power efficiency, stealthiness, and sustainability. It targets at the subframe level, accepts lower transmission power, and avoids cutting off the victim's connection to the cell while achieving a comparable attack success rate as the FBS and MITM. Seems impeccable; nonetheless, SigOver demands a relatively high precision in timing and frequency synchronization, which inspires us to


% By utilizing the policy loopholes and counterfeiting different broadcasting messages, SigOver produces various types of long-duration, sustainable attack goals as listed in Table \ref{sigOver}, continuously affecting individuals and groups without being aware.  In this section, we provide some necessary background to the SigOver attack.  First, we introduce two broadcasting messages and the corresponding Information Elements (IEs) that are assailable.  Then, we give a brief explanation of the attack model.

% \subsection{Cell Search and Synchronization}
% When a UE is turned on, it measures the RSSI over a set of candidate frequency channels and filters out candidates with low RSSI. After that, it obtains the primary synchronization signal (PSS) location to synchronize with the candidate cell in the time dimension. And then, the carrier frequency offset is estimated and compensated to synchronize in the frequency domain. The UE detects the radio frame timing, Physical Layer Cell ID, and cyclic prefix length from the secondary synchronization signal (SSS). Afterward, the UE reads the Master Information Block (MIB) and System information blocks (SIB) to check the Public Land Mobile Network (PLMN) identifier, camps on a cell, and reads other SIBs on that cell.

% We consider a cellular system with a legitimate BS, an attacker and a MS,
% shown in Fig.\ref{system_model}.

% % Figure: attackmodel
% \begin{figure}[ht]
% \centering
% \includegraphics[width=5cm]{fig/system_model.pdf}
% \caption{Attack a target UE by impersonating a legitimate BS  }
% \label{system_model}
% \end{figure}

% The attacker is supposed to transmit its malicious data stream $x_2$, impersonating a legitimate one $x_1$,
% to the MS at the same time with the same carrier frequencies as the BS.
% In other words, we assume the malicious signals is a synchronized signa
% We use $h_i$ and $H_i$ to represent the downlink channels before and after FFT.
% Subscript $1$ is from the BS to the MS and subscript $2$ from the attacker.
% And the channel is estimated by the reference signals (pilots).


% \subsection{Targeted messages}
% \subsubsection{System Information Block (SIB)}
% Among all the SIB, SIB1 and SIB2 are essential for UE to camp on a specific cell. A moving UE retrieves the Tracking Area Code (TAC) in the SIB1 of the cell and validates it with its TAI list to identify a nearby new base station to hand over for better service. Repeating fake TAC not on the TAI list triggers UE sending TAU requests and thus traps UE in the TAU process forever. The BarringFactor parameter in SIB2 limits the number of UE that can access the network. SigOver attacker can exploit this parameter to block a group of UE by setting it to 0.

% \subsubsection{Paging}
% Another vulnerable shared message is paging which broadcasts downlink arrival, emergency alert, or system information change in a time window with a fixed cycle.  Following IEs in the paging message can be leveraged to achieve different attacks.

% \begin{itemize}
% \item Indentity: Each UE has a permanent identity, International Mobile Subscriber Identity (IMSI), and a temporary identity, S-TMS, assigned by the cell.  Paging with IMSI usually implies a network or an S-TMSI  allocating failure; by injecting paging messages with IMSI repetitively, SigOver can keep a UE reattaching a cell to achieve  (Denial of Service) DoS attack.  In addition,  SigOver can trace a victim's coarse-grained geolocation by forcing the UE to set up a connection to the Network with its S-TMSI and eavesdropping on the Connection setup messages from the legitimate cell.
% \item Core-network domain: SigOver can downgrade the victim UEs to 3G Network by replacing Packet Switched (PS) Service indicates a data transfer or an incoming SMS with Circuit Switched (CS) notifies a fall-back speech call service in a paging message
% \item ETWS:  SigOver can false alarm emergency alerts via Earthquake and Tsunami Warning System (ETWS) and Commercial Mobile Alert System (CMAS) in a paging message
% \item System information modification: SigOver can notify victims to read modified SIB1 by setting the field to be true in a paging message.
% \end{itemize}


% SigOver can be used to accomplish diverse attack goals by crafting particular Information Elements (IEs) in corresponding subframes, as shown in Table \ref{SigOver}.  One target message is System Information Blocks.  Before handing over to a new cell, a UE needs to indentify the cell by retrieving the Tracking Area Code (TAC) in the SIB1 and validating it with its TAI list.  Repeating fake TAC that is not on the TAI list triggers UE sending TAU requests and thus traps UE in the TAU process forever.  SigOver attacker can exploit the BarringFactor parameter in SIB2, limiting the number of UE that can access the Network to block a group of UE by setting it to 0.  Another vulnerable shared message is paging which broadcasts downlink arrival, emergency alert, or system information change in a time window with a fixed cycle.  Each UE has a permanent identity, International Mobile Subscriber Identity (IMSI), and a temporary identity, S-TMS, assigned by the cell.  Paging with IMSI usually implies a network or an S-TMSI allocating failure; by injecting paging messages with IMSI repetitively, SigOver can keep a UE reattaching a cell to achieve (Denial of Service) DoS attack.  In addition, SigOver can trace a victim's coarse-grained geolocation by forcing the UE to set up a connection to the Network with its S-TMSI and eavesdropping on the Connection setup messages from the legitimate cell.  SigOver can downgrade the victim UEs to 3G Network by replacing Packet Switched (PS) Service indicates a data transfer or an incoming SMS with Circuit Switched (CS) notifies a fall-back speech call service in a paging message.  SigOver can false alarm emergency alerts via Earthquake and Tsunami Warning System (ETWS) and Commercial Mobile Alert System (CMAS) in a paging message. SigOver can notify victims to read modified SIB1 by setting the field true in a paging message.

% %To provide context for our work, in this section, we first provide a brief overview of Sigover~\cite{sigover} as the first successful signal injection attack in cellular networks and the attack we use to evaluate \T{\Name{}}. We then provide a formalization of channel estimation, which we will use in describing the \T{\Name{}} approach in Section~\ref{sec:design}. Finally we describe wireless channel metrics that have been used in earlier work to detect wireless attacks.

% %first introduced SigOver and channel estimation. Then, we analyzed those commonly used approaches for identifying active attackers in wireless communication, their difficulties in detecting signal injection attacks, and the potential of using channel estimation for \T{\Name{}}.%Then we explain the need for a collaborative endpoint-based detection method. Next, we study and compare RSSI and channel frequency response measurements as potential detection metrics.




% \begin{figure}[htpb]
%      \centering
%      \includegraphics[width=0.45\textwidth]{fig/observation.png}
%      \caption{}
%      \label{observation}
% \end{figure}

% Figure \ref{observation} presents the results of an The figure displays the original frequency responses of a legitimate channel and an attacked channel, as well as how the frequency response of the legitimate channel changes when subjected to various timing offsets during the attack. the attack can go undetected by the UE. However, as the timing offset increases, specific patterned fluctuations appear in the frequency response amplitudeHence, there is a need for a simple detection and localization method that leverages the available channel information with minimal computational demands through a crowd-sourcing approach.This approach involves exchanging lightweight information and keys for efficient detection and localization while minimizing payload strength.

% \fi
